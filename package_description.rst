Simple Zuora Client

``Version 0.1.6``

It works with Zuora® API v1.

*Currently supported features*

* Basic get, post, put, delete
* Authentication session
* Multi entity support
* AQuA batch queries (ZOQL)
* query jobs (OWL queries)
* get accounting periods
* get sequence sets
* delete a sequence set
* get invoice by invoice number
* update account
* update credit memo
* cancel credit memo
* update invoice

*Requirements*

* requests_oauthlib
* oauthlib.oauth2
* requests
